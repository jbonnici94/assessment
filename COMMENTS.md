# README #

Clear Settle Assessment.

### How do I get set up? ###

* create a schema in mysql called "clearsettle_assessment" (or change this to your preferred name in the POM file and also in the application.properties file).
* open cmd in main folder (where pom.xml is) and execute "mvn flyway:migrate". This uses the "root" user having password as "password". You can change this from the POM file "flywaydb" section.
* database should be set up automatically.
* change the application.properties if necessary and execute "mvn clean install"
* go to the target folder and run "java -jar assessment-1.1.1.jar"
*
* APIs:
* register (POST)
  localhost:8080/api/user/register
    {
      "name": "James",
      "surname": "Bonnici",
      "accountName": "Main Account",
      "cardNumber": 123456789
    }

* get card numbers (GET)
  localhost:8080/api/user/get/card-numbers?id={value}
  where {value} is the ID of the User. This should return all Card Numbers for the user.

### Contribution guidelines ###

* Since I was tasked to work with one card number, I didn't implement the API where the user can create multiple Accounts although this is already supported in the User Service.
* I developed the "get card numbers" API to accept id of a user. In the real-life application I would have assigned a token to a user (after logging in), then for each call I would make an AOP which retrieves the object user from that token. This would then be supplied to all methods containing a User object and work on this object. This will ensure that only the data for the current user is outputted.
example of concept: (changing the method to accept a User object as parameter and removing the id from parameter)
  public CardNumberResponseApi getCardNumber(User user)
  
in the AOP the application would check for all params in the method:
    Object[] currArgs = pjp.getArgs();

    //if a User instance exists in methods, then pass the current user.
    for (int i = 0; i < currArgs.length; i++){
        if (currArgs[i] instanceof User){
            currArgs[i] = user; //user retrieved from token
            break;
        }
    }
    
    //proceed to the API call with the modified arguments
    Object result = pjp.proceed(currArgs);

### Who do I talk to? ###

* James Bonnici (jbonnici94@gmail.com)