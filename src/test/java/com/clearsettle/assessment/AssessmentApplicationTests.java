package com.clearsettle.assessment;

import com.clearsettle.assessment.user.dao.User;
import com.clearsettle.assessment.user.dao.UserAccount;
import com.clearsettle.assessment.user.dao.repository.UserRepository;
import com.clearsettle.assessment.user.service.UserService;
import com.clearsettle.assessment.user.service.UserServiceImpl;
import com.clearsettle.assessment.util.AppUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class AssessmentApplicationTests {

	private static Long cardNumber = 123456789L;

	private static UserService userService;

	private static UserRepository userRepository;

	@BeforeClass
	public static void initTests(){
		userService = new UserServiceImpl();
		userRepository = mock(UserRepository.class);
	}

	@Test
	public void getUserCards(){
		when(userRepository.findOne(any(Long.class))).thenReturn(createUser());
		ReflectionTestUtils.setField(userService, "userRepository", userRepository);

		List<Long> cards = userService.getCards(1L);

		verify(userRepository, times(1)).findOne(any(Long.class));
		assertEquals(cards.get(0), cardNumber);
	}

	private User createUser(){
		List<UserAccount> accounts = new ArrayList<>();
		UserAccount account = new UserAccount();

		account.setId(1L);
		account.setName("Main Account");
		account.setCardNumber(AppUtils.encrypt(cardNumber));

		accounts.add(account);

		User user = new User();

		user.setId(1L);
		user.setName("James");
		user.setSurname("Bonnici");

		user.setAccounts(accounts);
		account.setUser(user);

		return user;
	}
}
