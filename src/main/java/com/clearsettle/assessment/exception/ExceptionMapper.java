package com.clearsettle.assessment.exception;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by jamesb on 01/07/2017.
 */
@ControllerAdvice
public class ExceptionMapper extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        Map<String, String> errorMessages = fieldErrors
                .parallelStream()
                .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
        Map<String, Object> errors = new HashMap<>();
        errors.put("errors", errorMessages);
        return constructResponse(ex, request, HttpStatus.BAD_REQUEST, errors);
    }

    @ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="Bad Request")
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        Map<String, String> errorMessages = fieldErrors
                .parallelStream()
                .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
        Map<String, Object> errors = new HashMap<>();
        errors.put("errors", errorMessages);
        return constructResponse(ex, request, HttpStatus.BAD_REQUEST, errors);
    }

    @ResponseStatus(value=HttpStatus.CONFLICT, reason="Data sent has already been inserted in database.")
    @ExceptionHandler(DataIntegrityViolationException.class)
    public void handleDataIntegrityViolationException(){
        logger.error("DataIntegrity handler executed");
    }

    private ResponseEntity<Object> constructResponse(Exception e, WebRequest request, HttpStatus status, Map<String, Object> errors) {
        HttpHeaders errorHeaders = constructHttpHeaders();
        return handleExceptionInternal(e, errors, errorHeaders, status, request);
    }

    private HttpHeaders constructHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }
}
