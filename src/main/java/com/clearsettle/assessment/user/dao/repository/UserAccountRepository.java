package com.clearsettle.assessment.user.dao.repository;

import com.clearsettle.assessment.user.dao.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jamesb on 01/07/2017.
 */
@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {
}
