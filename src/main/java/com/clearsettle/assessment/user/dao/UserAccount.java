package com.clearsettle.assessment.user.dao;

import javax.persistence.*;

/**
 * Created by jamesb on 01/07/2017.
 */
@Entity(name = "user_account")
public class UserAccount {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @Column(name = "card_number")
    private String cardNumber;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
