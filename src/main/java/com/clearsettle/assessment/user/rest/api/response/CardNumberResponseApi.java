package com.clearsettle.assessment.user.rest.api.response;

import java.util.List;

/**
 * Created by jamesb on 01/07/2017.
 */
public class CardNumberResponseApi {

    private List<Long> cardNumbers;

    public List<Long> getCardNumbers() {
        return cardNumbers;
    }

    public void setCardNumbers(List<Long> cardNumbers) {
        this.cardNumbers = cardNumbers;
    }
}
