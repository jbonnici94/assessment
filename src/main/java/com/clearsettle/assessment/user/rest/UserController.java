package com.clearsettle.assessment.user.rest;

import com.clearsettle.assessment.user.rest.api.request.RegisterRequestApi;
import com.clearsettle.assessment.user.rest.api.response.CardNumberResponseApi;
import com.clearsettle.assessment.user.rest.api.response.RegisterResponseApi;
import com.clearsettle.assessment.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by jamesb on 01/07/2017.
 */
@RestController
@RequestMapping("/api/user/")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * Registers a new user to the system
     * @param request Including User Details and One Account Details
     * @return The generated ID of the new user
     */
    @RequestMapping(value = "register", method = RequestMethod.POST)
    public RegisterResponseApi register(@RequestBody @Valid RegisterRequestApi request){
        RegisterResponseApi response = new RegisterResponseApi();

        response.setId(userService.register(request));

        return response;
    }

    /**
     * Retrieve all cards for the particular User
     * @param id ID of User
     * @return Card Numbers
     */
    @RequestMapping(value = "get/card-numbers", method = RequestMethod.GET)
    public CardNumberResponseApi getCardNumber(@RequestParam Long id){
        CardNumberResponseApi response = new CardNumberResponseApi();

        response.setCardNumbers(userService.getCards(id));

        return response;
    }
}
