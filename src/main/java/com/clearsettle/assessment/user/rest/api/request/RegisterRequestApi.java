package com.clearsettle.assessment.user.rest.api.request;

import javax.validation.constraints.NotNull;

/**
 * Created by jamesb on 01/07/2017.
 */
public class RegisterRequestApi {
    @NotNull(message = "Name cannot be left empty.")
    private String name;

    @NotNull(message = "Surname cannot be left empty.")
    private String surname;

    @NotNull(message = "Account Name cannot be left empty.")
    private String accountName;

    @NotNull(message = "Card Number cannot be left empty.")
    private Long cardNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Long cardNumber) {
        this.cardNumber = cardNumber;
    }
}
