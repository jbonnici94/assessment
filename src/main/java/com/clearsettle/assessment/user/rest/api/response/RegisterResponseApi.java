package com.clearsettle.assessment.user.rest.api.response;

/**
 * Created by jamesb on 01/07/2017.
 */
public class RegisterResponseApi {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
