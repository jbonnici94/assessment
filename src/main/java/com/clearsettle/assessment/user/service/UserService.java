package com.clearsettle.assessment.user.service;

import com.clearsettle.assessment.user.dao.User;
import com.clearsettle.assessment.user.rest.api.request.RegisterRequestApi;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

/**
 * Created by jamesb on 01/07/2017.
 */
public interface UserService {
    Long register(RegisterRequestApi request);
    List<Long> getCards(Long id);
    Long createAccount(User user, String name, Long cardNumber);
}
