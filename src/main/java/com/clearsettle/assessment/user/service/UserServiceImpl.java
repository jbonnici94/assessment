package com.clearsettle.assessment.user.service;

import com.clearsettle.assessment.user.dao.User;
import com.clearsettle.assessment.user.dao.UserAccount;
import com.clearsettle.assessment.user.dao.repository.UserAccountRepository;
import com.clearsettle.assessment.user.dao.repository.UserRepository;
import com.clearsettle.assessment.user.rest.api.request.RegisterRequestApi;
import com.clearsettle.assessment.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jamesb on 01/07/2017.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserAccountRepository accountRepository;

    /**
     * Registers a new user
     * @param request Details of the user
     * @return the ID of the newly created User
     */
    @Override
    public Long register(RegisterRequestApi request) {
        User user = new User();

        user.setName(request.getName());
        user.setSurname(request.getSurname());

        userRepository.save(user);

        createAccount(user, request.getAccountName(), request.getCardNumber());

        return user.getId();
    }

    /**
     * Creates a new account for the specified user.
     * @param user User that the account is to be created for.
     * @param name Name for the account
     * @param cardNumber Card for the Account
     * @return The ID for the newly generated Account
     */
    @Override
    public Long createAccount(User user, String name, Long cardNumber){
        UserAccount account = new UserAccount();

        account.setName(name);
        account.setCardNumber(AppUtils.encrypt(cardNumber));
        account.setUser(user);

        accountRepository.save(account);

        return account.getId();
    }

    /**
     * Retrieve all Card Numbers for the User
     * @param id ID of the User
     * @return a list of card numbers
     */
    @Override
    public List<Long> getCards(Long id) {
        User user = userRepository.findOne(id);

        List<Long> cards = user.getAccounts().stream().map(account ->{
            return Long.valueOf(AppUtils.decrypt(account.getCardNumber()));
        }).collect(Collectors.toList());

        return cards;
    }
}
