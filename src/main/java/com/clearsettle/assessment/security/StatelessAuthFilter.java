package com.clearsettle.assessment.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jamesb on 01/07/2017.
 */
@Component
public class StatelessAuthFilter implements Filter {
    private static final String HEADER_TOKEN = "X-AUTH-TOKEN";

    @Value("${web.services.api.key}")
    private String apiKey;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String authToken = request.getHeader(HEADER_TOKEN); //Retrieving Token from Header

        /*
        If key in header does not match the one in the properties file
        AND
        If the API call is not to register a new user,
        The application will return an Unauthorized Error response to the client application
        */
        if ((StringUtils.isEmpty(authToken) || !apiKey.equals(authToken))
                && !request.getRequestURI().equalsIgnoreCase("/api/user/register")){
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid API Key.");
        } else {
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
