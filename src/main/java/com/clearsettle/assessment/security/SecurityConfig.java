package com.clearsettle.assessment.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.ExceptionTranslationFilter;

/**
 * Created by jamesb on 01/07/2017.
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private RestAuthEntryPoint entryPoint;

    @Autowired
    private StatelessAuthFilter authFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //disabling default Spring CSRF and calling our custom Security Filtering.
        http.addFilterAfter(authFilter, ExceptionTranslationFilter.class)
                .csrf().disable()
                .authorizeRequests()
                .anyRequest().permitAll();

        http.httpBasic().authenticationEntryPoint(entryPoint);
    }
}
