package com.clearsettle.assessment.util;

import org.jasypt.util.text.BasicTextEncryptor;

/**
 * Created by jamesb on 01/07/2017.
 */
public class AppUtils {
    private static BasicTextEncryptor encryptor = new BasicTextEncryptor();

    static{
        //dummy password just for the sake of this assignment
        encryptor.setPassword("123456");
    }
    public static String encrypt(Long data){
        return encrypt(data.toString());
    }

    public static String encrypt(String data){
        return encryptor.encrypt(data);
    }

    public static String decrypt(String data){
        return encryptor.decrypt(data);
    }
}
